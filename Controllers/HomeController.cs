﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clusterization;
using System.Threading;

namespace ImageLoading.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            string fileName = null;
            if (upload != null)
            {
                fileName = System.IO.Path.GetFileName(upload.FileName);
                upload.SaveAs(Server.MapPath("~/Files/" + fileName));
                ImagePostprocessing image = new ImagePostprocessing(Server.MapPath("~/Files/" + fileName));
                image.ColorCorrection();
                //win form doesn't overflow stack but in asp this problem exist
                Thread thread = new Thread(new ThreadStart(image.FindChainedAreas), 10485760);      //10MB stack
                thread.Start();

                while (!thread.IsAlive) ;               //wait thread activates
                thread.Join();

                List<string> list = new List<string>();
                list.Add("/Files/" + fileName);
                list.Add("/Files/" + fileName.Insert(fileName.IndexOf('.'), "_changed"));
                return View(list);
            }
            return View();
        }
    }
}
