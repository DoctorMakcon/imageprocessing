﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clusterization
{
    class Characteristics
    {
        public int square { get; set; }
        public int perimeter { get; set; }
        public double density { get; set; }
        public CenterMass centerMass { get; set; }
        public double elongation { get; set; }

        public int _class { get; set; }

        public Characteristics()
        {
            centerMass = new CenterMass();
        }

        public void CalculateDensity()
        {
            density = perimeter * perimeter / square;
        }

        public int CompareTo(Characteristics obj)
        {
            if (obj == null)
                return 1;
            else return this.square.CompareTo(obj.square);
        }
    }
}