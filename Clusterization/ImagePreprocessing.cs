﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Clusterization
{
    class ImagePreprocessing
    {
        public static void MedianFilter(Bitmap image)
        {
            for (int y = 1; y < image.Height - 1; y++)
                for (int x = 1; x < image.Width - 1; x++)
                    CalculateAverage(image, x, y);
        }

        private static void CalculateAverage(Bitmap image, int x, int y)
        {
            List<Color> list = new List<Color>();
            for (int i = y - 1; i < y + 2; i++)
                for (int j = x - 1; j < x + 2; j++)
                    list.Add(image.GetPixel(j, i));

            list.Sort(delegate(Color obj1, Color obj2)
            {
                return obj1.GetBrightness().CompareTo(obj2.GetBrightness());
            });
            image.SetPixel(x, y, list[4]);
        }

        public static void GammaCorrection(Bitmap image)
        {
            double c = 2.60;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    Color current = image.GetPixel(x, y);

                    int red = (int)(current.R * c);
                    red = red > 255 ? 255 : red;
                    int green = (int)(current.G * c);
                    green = green > 255 ? 255 : green;
                    int blue = (int)(current.B * c);
                    blue = blue > 255 ? 255 : blue;

                    Color clr = Color.FromArgb(255, red, green, blue);
                    image.SetPixel(x, y, clr);
                }
            }
        }

        public static void CheckImageBrightness(Bitmap image, float brightnessMedian)
        {
            double brightPixelsCount = 0;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (image.GetPixel(x, y).GetBrightness() >= brightnessMedian)
                        brightPixelsCount++;
                }
            }
            double totalPixelsCount = image.Height * image.Width;

            double percent = (brightPixelsCount / totalPixelsCount) * 100;
            if (percent < 3)
                GammaCorrection(image);
        }
    }
}