﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Clusterization
{
    class ImagePostprocessing
    {
        public Bitmap image { get; set; }
        public string sourceImage;

        private Dictionary<int, Characteristics> dictionary;

        private Color white = Color.FromArgb(255, 255, 255, 255);
        private Color black = Color.FromArgb(255, 0, 0, 0);
        private Color median = Color.FromArgb(255, 210, 210, 210);
        //private Color median = Color.FromArgb(255, 170, 170, 170);

        public Characteristics first { get; set; }
        public Characteristics second { get; set; }

        public ImagePostprocessing()
        {
            if (string.IsNullOrWhiteSpace(sourceImage))
                image = new Bitmap("d:\\projects\\C#\\ASP\\ImageLoading\\Files\\P8.jpg");
            else image = new Bitmap(sourceImage);
            dictionary = new Dictionary<int, Characteristics>();
            first = null;
            second = null;
        }

        public ImagePostprocessing(string pathToImage)
        {
            sourceImage = pathToImage;
            image = new Bitmap(sourceImage);
            dictionary = new Dictionary<int, Characteristics>();
        }

        private int[][] _labels;
        public int[][] labels
        {
            set { }
            get
            {
                if (_labels == null)
                {
                    _labels = new int[image.Width][];
                    for (int i = 0; i < image.Width; i++)
                        _labels[i] = new int[image.Height];
                }
                return _labels;
            }
        }

        public float brightnessMedian { get; set; }
        public float brightnessWhite { get; set; }
        public float brightnessBlack { get; set; }

        public void ColorCorrection()
        {
            if (image == null)
                return;
            if (brightnessMedian == 0)
            {
                brightnessMedian = median.GetBrightness();
                brightnessWhite = white.GetBrightness();
                brightnessBlack = black.GetBrightness();
            }

            ImagePreprocessing.CheckImageBrightness(image, brightnessMedian);
            ImagePreprocessing.MedianFilter(this.image);

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    if (image.GetPixel(i, j).GetBrightness() < brightnessMedian)
                        image.SetPixel(i, j, black);
                    else image.SetPixel(i, j, white);
                }
            }
        }

        public void FindChainedAreas()
        {
            int L = 1;
            for (int y = 0; y < image.Height; y++)                  //find chained areas
            {
                for (int x = 0; x < image.Width; x++)
                {
                    Fill(x, y, L++);
                }
            }

            for (int y = 0; y < image.Height; y++)                  //calc perimeter & square
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (labels[x][y] != 0)
                    {
                        dictionary[labels[x][y]].square++;
                        if (IsBorder(x, y))
                            dictionary[labels[x][y]].perimeter++;
                    }
                }
            }

            CalculateCenterMass();
            CalculateElongation();

            int firstPos = 0, secondPos = 0;                //get random positions for clusters center
            Random rnd = new Random();
            while (firstPos == secondPos)
            {
                firstPos = rnd.Next(dictionary.Count);
                secondPos = rnd.Next(dictionary.Count);
            }

            int pos = 0;
            foreach (var obj in dictionary)
            {
                obj.Value.CalculateDensity();
                if (pos == secondPos)
                    second = obj.Value;
                if (pos == firstPos)
                    first = obj.Value;
                pos++;
            }

            bool result = DeleteSingleDots();

            while (true)                                            //calculate distance between cluster centers & chained areas
            {
                foreach (var obj in dictionary)
                {
                    double betweenFirst = CalculateDistance(obj.Value, first);
                    double betweenSecond = CalculateDistance(obj.Value, second);
                    obj.Value._class = (betweenFirst < betweenSecond) ? 1 : 2;
                }
                if (RecalculateClusterCenters())
                    break;
            }

            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (labels[x][y] != 0)
                    {
                        Characteristics temp = null;
                        if (dictionary.TryGetValue(labels[x][y], out temp))
                        {
                            if (temp._class == 1)
                                image.SetPixel(x, y, Color.DarkSeaGreen);
                            else image.SetPixel(x, y, Color.Red);
                            if (temp.square > 3950 && result)
                                image.SetPixel(x, y, Color.Aqua);
                        }
                        else image.SetPixel(x, y, black);
                    }
                }
            }

            string newFileName = this.sourceImage.Insert(this.sourceImage.IndexOf('.'), "_changed");
            this.image.Save(newFileName);
            image.Dispose();
        }

        private void Fill(int x, int y, int L)
        {
            if ((labels[x][y] == 0) && (image.GetPixel(x, y).GetBrightness() == brightnessWhite))
            {
                labels[x][y] = L;

                if (!dictionary.Keys.Contains(L))
                    dictionary.Add(L, new Characteristics());

                if (x > 0)
                    Fill(x - 1, y, L);

                if (x < image.Width - 1)
                    Fill(x + 1, y, L);

                if (y > 0)
                    Fill(x, y - 1, L);

                if (y < image.Height - 1)
                    Fill(x, y + 1, L);
            }
        }

        private bool IsBorder(int x, int y)
        {
            if (x > 0 && labels[x - 1][y] == 0)
                return true;
            if (y > 0 && labels[x][y - 1] == 0)
                return true;
            if (x != (image.Width - 1) && labels[x + 1][y] == 0)
                return true;
            if (y != (image.Height - 1) && labels[x][y + 1] == 0)
                return true;
            return false;
        }

        private double CalculateDistance(Characteristics target, Characteristics center)
        {
            return Math.Sqrt(Math.Pow((target.perimeter - center.perimeter), 2) +
                Math.Pow((target.square - center.square), 2) +
                Math.Pow((target.density - center.density), 2));
        }

        private bool RecalculateClusterCenters()
        {
            Characteristics tempFirst = first, tempSecond = second;
            List<Characteristics> firstList = new List<Characteristics>();
            List<Characteristics> secondList = new List<Characteristics>();
            foreach (var obj in dictionary)
            {
                if (obj.Value._class == 1)
                    firstList.Add(obj.Value);
                else secondList.Add(obj.Value);
            }

            firstList.Sort(delegate(Characteristics obj1, Characteristics obj2)
            {
                return obj1.CompareTo(obj2);
            });
            secondList.Sort(delegate(Characteristics obj1, Characteristics obj2)
            {
                return obj1.CompareTo(obj2);
            });

            if (firstList.Count != 0)
                this.first = firstList[firstList.Count / 2];
            if (secondList.Count != 0)
                this.second = secondList[secondList.Count / 2];

            return (tempFirst == this.first && tempSecond == this.second) ? true : false;
        }

        private bool DeleteSingleDots()
        {
            bool result = false;
            List<int> keysForDelete = new List<int>();
            var keys = dictionary.Keys;
            foreach (var k in keys)
            {
                if (dictionary[k].square < 10)
                {
                    result = true;
                    keysForDelete.Add(k);
                }
            }
            foreach (int k in keysForDelete)
                dictionary.Remove(k);
            return result;
        }

        private void CalculateCenterMass()
        {
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (labels[x][y] != 0)
                    {
                        dictionary[labels[x][y]].centerMass.x += x * image.GetPixel(x, y).GetBrightness();
                        dictionary[labels[x][y]].centerMass.y += y * image.GetPixel(x, y).GetBrightness();
                    }
                }
            }

            foreach (var obj in dictionary)
            {
                obj.Value.centerMass.x /= obj.Value.square;
                obj.Value.centerMass.y /= obj.Value.square;
            }
        }

        private void CalculateElongation()
        {
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (labels[x][y] != 0)
                    {
                        dictionary[labels[x][y]].centerMass.m20 +=
                            Math.Pow(x - dictionary[labels[x][y]].centerMass.x, 2) * image.GetPixel(x, y).GetBrightness();
                        dictionary[labels[x][y]].centerMass.m02 +=
                            Math.Pow(y - dictionary[labels[x][y]].centerMass.y, 2) * image.GetPixel(x, y).GetBrightness();
                        dictionary[labels[x][y]].centerMass.m11 +=
                            (x - dictionary[labels[x][y]].centerMass.x) * (y - dictionary[labels[x][y]].centerMass.y) * image.GetPixel(x, y).GetBrightness();
                    }
                }
            }

            foreach (var obj in dictionary)
            {
                CenterMass c = obj.Value.centerMass;
                obj.Value.elongation = (c.m20 + c.m02 + Math.Sqrt(Math.Pow(c.m20 - c.m02, 2) + 4 * c.m11 * c.m11)) /
                                        c.m20 + c.m02 - Math.Sqrt(Math.Pow(c.m20 - c.m02, 2) + 4 * c.m11 * c.m11);
            }
        }
    }
}
